// Code goes here

(function ($) {

    var rules = {
        required_union: {
            check: function (value, another) {
                return Boolean(value) || Boolean(another);
            },
            message: "Заполните хотя бы одно из полей"
        },
        required: {
            check: function (value) {
                return Boolean(value);
            },
            message: "Поле обязательное"
        },
        password: {
            check: function (value, passwordLength) {
                return value.length > passwordLength;
            },
            message: "Пароль слишком простой"
        },
        match: {
            check: function (value, matchValue) {
                return value == matchValue;
            },
            message: "Два поля должны совпадать"
        },
        email: {
            check: function(value){
                if (value.length > 0) {
                    var re = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})$/;
                    return re.test(value);
                }
                return true;
            },
            message: "Введите адрес правильно"
        },
        phone: {
            check: function(value){
                if (value.length > 0) {
                    var re = /^\+?\d{7,12}$/;
                    return re.test(value);
                }
                return true;
            },
            message: "Введите телефон правильно"
        }
    };

    // ---- Model ----

    var ModelClass = function () {
        this.data = {};
    };
    ModelClass.prototype.validate = function () {
        var allValid = true;
        for (var inputName in this.data) {
            var input = this.data[inputName];
            for (var i = 0; i < input.rules.length; i++) {
                var rule = input.rules[i];
                var ok = rules[rule.name].check(input.value, rule.arg);
                if (!ok) {
                    allValid = false;
                    input.error = true;
                    input.message = rules[rule.name].message;
                }
            }
        }
        return allValid;
    };
    ModelClass.prototype.update = function (data) {
        $.extend(this.data, data);
    };

    // ---- View ----

    var ViewClass = function (form) {
        this.$form = $(form);
        this.$form.attr("novalidate", true);
    };
    ViewClass.prototype.getData = function () {
        var self = this,
            data = {};
        this.$form.find(':input').each(function (i, input) {
            var name = $(input).attr('name');
            var validators = [];
            // get validators from html markup
            if ($(input).data('validationPassword')) {
                validators.push({
                    name: 'password',
                    arg: $(input).data('validationPassword')
                });
            }
            if ($(input).data('validationMatch')) {
                validators.push({
                    name: 'match',
                    arg: self.$form.find('input[name=' + $(input).data('validationMatch') + ']').val()
                });
            }
            if ($(input).attr('required')) {
                validators.push({
                    name: 'required'
                });
            }
            if ($(input).attr("type") === "email") {
                validators.push({
                    name: 'email'
                });
            }
            if ($(input).data('validationPhone')) {
                validators.push({
                    name: 'phone'
                });
            }
            if ($(input).data('requiredUnion')) {
                validators.push({
                    name: 'required_union',
                    arg: self.$form.find('input[name=' + $(input).data('requiredUnion') + ']').val()
                });
            }
            var value = $(input).val();
            if ($(input).attr("type") === "checkbox")
                value = $(input).prop("checked");
            data[name] = {
                value: value,
                rules: validators
            };
        });
        return data;
    };
    ViewClass.prototype.onChange = function (callback) {
        this.$form
            .find('input')
            .on('blur', callback)
            .on('keyup', callback)
            .on('change', callback);
    };
    ViewClass.prototype.onSubmit = function (callback) {
        this.$form
            .on('submit', callback);
    };
    ViewClass.prototype.highlight = function (name, message) {
        this.$form
            .find('[name=' + name + ']')
            .parents('.form-group')
            .addClass('has-error')
            .append('<p class="text text-danger">' + message + '</p>');
    };
    ViewClass.prototype.unHighlight = function (name) {
        this.$form
            .find('[name=' + name + ']')
            .parents('.form-group')
            .removeClass("has-error")
            .find("p.text-danger").remove();
    };


    // ---- Controller ----
    var Validator = function (formElement) {
        this.view = new ViewClass(formElement);
        this.model = new ModelClass();
        this.setup();
    };
    Validator.prototype.setup = function () {
        var self = this; // closure
        this.view.onChange(function (e) {
            self.process(e);
        });
        this.view.onSubmit(function (e) {
            return self.process();
        });
    };
    Validator.prototype.process = function (e) {
        var self = this;
        var viewData = this.view.getData();
        this.model.update(viewData);
        var isValid = this.model.validate();
        var processHighlight = function(name){
            var item = self.model.data[name];
            self.view.unHighlight(name);
            if (item.error) {
                self.view.highlight(name, item.message);
            }
        };
        if (e === undefined)
            for (var name in this.model.data)
                processHighlight(name);
        else
            processHighlight($(e.target).attr("name"));
        return isValid;
    };

    // ---- jQuery plugin setup ----

    $.fn.validate = function () {
        return $(this).each(function (i, form) {
            var validator;
            if (!$(form).data('validator')) {
                validator = new Validator(form);
                $(form).data('validator', validator);
            }
        });
    };

    // ---- plugin apply ----

    $(function () {
        $('form').validate();
    });

})(jQuery);