describe('jQuery plugin simple suite', function () {
    var element;

    beforeEach(function () {
        element = $('<form><div class="form-group">' +
            '<label class="control-label">Username</label>' +
            '<input name="username" type="text" class="form-control" id="id_username" required/>' +
            '</div></form>');
        element.validate();
    });

    it('validator binds to data property', function () {
        expect(element).toHaveData('validator');
    });

    it('disable vrowser validation', function () {
        expect(element).toHaveAttr('novalidate');
    });

    it("don't validate right on start", function () {
        expect(element.find('.form-group')).not.toHaveClass('has-error');
    });

    it('highlight error if username empty', function () {
        element.find('input').trigger('blur');
        expect(element.find('.form-group')).toHaveClass('has-error');
    });

    it('correclty process required rule from view', function () {
        var validator = element.data('validator');
        expect(validator.view.getData().username.rules).toEqual([{
            name: 'required'
        }]);
    });

    it('shows error message in red text', function () {
        element.find('input').trigger('blur');
        expect(element).toContainElement('.text');
        expect(element.find('.text')).toHaveClass('text-danger');
    });

    it('remove error text and highlight when field valid', function () {
        element.find('input').trigger('blur');
        element.find('input').val('test');
        element.find('input').trigger('blur');
        expect(element).not.toContainElement('.text');
        expect(element.find('.form-group')).not.toHaveClass('has-error');
    });

    it('prevent submit if form not valid', function () {
        // jQuery hack
        // http://stackoverflow.com/questions/2518421/jquery-find-events-handlers-registered-with-an-object
        var submitHandler = $._data(element[0], 'events').submit[0].handler;
        submitResult = submitHandler();
        expect(submitResult).toBe(false);
    });
});

describe('Small checkbox suite', function () {
    var element;

    beforeEach(function () {
        element = $('<form><div class="form-group">' +
            '<label class="control-label">Username' +
            '<input name="eula" type="checkbox" class="form-control" id="id_eula" required/>' +
            '</label></div></form>');
        element.validate();
    });

    it('correclty process checkbox value', function () {
        var validator = element.data('validator');
        expect(validator.view.getData().eula.value).toBe(false);
    });
});



describe('Two fields suite', function () {
    var element;

    beforeEach(function () {
        element = $('<form>' +
            '<div class="form-group">' +
            '<label class="control-label">Password' +
            '<input name="password" type="password" class="form-control" id="id_password" data-validation-password="6" required/>' +
            '</label></div>' +
            '<div class="form-group">' +
            '<label class="control-label">Repeat password' +
            '<input name="password2" type="password" class="form-control" id="id_password2" data-validation-match="password" required />' +
            '</label></div>' +
            '</form>');
        element.validate();
    });

    it('do not highlight all fields if one changed', function () {
        element.find('#id_password').trigger('blur');
        expect(element.find('.has-error').length).toEqual(1);
    });

    it('correclty parse password validation markup', function () {
        var validator = element.data('validator');
        expect(validator.view.getData().password.rules).toEqual([{
            name: 'password',
            arg: 6
        }, {
            name: 'required'
        }]);
    });

    it('correctly parse match validation markup', function () {
        element.find('input#id_password').val('password123');
        var validator = element.data('validator');
        expect(validator.view.getData().password2.rules).toEqual([{
            name: 'match',
            arg: 'password123'
        }, {
            name: 'required'
        }]);
    });
});